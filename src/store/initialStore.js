const initialStore = {
    goods: [],
    cart: [],
    favorites: [],
    isAddToCartModalOpen: "",
    isRemoveFromCartModalOpen: "",
    checkoutForm: {}
};

export default initialStore;