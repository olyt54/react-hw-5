import React from 'react';
import propTypes, {oneOfType} from "prop-types"
import "./button.scss"

const Button = ({text, bgColor, handleBtn, className, type}) => {
    return (
        <button
            className={className ? `btn ${className}` : "btn"}
            style={{backgroundColor: bgColor}}
            onClick={!type ? () => handleBtn() : null}
            type={type}
        >
            {text}
        </button>
    );
}

Button.propTypes = {
    text: oneOfType([
        propTypes.string,
        propTypes.element
    ]).isRequired,
    bgColor: propTypes.string,
    handleBtn: propTypes.func.isRequired
}

export default Button;